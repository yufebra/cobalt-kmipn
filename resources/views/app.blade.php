<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

{{--    <title>{{ config('app.name', 'Laravel') }}</title>--}}
    <title>SUCCESS Computer Based Test Simulation System - SUCCESS COBALT</title>
    <link rel="icon" href="https://success.bateh.web.id/img/success.png">

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    <!-- Scripts -->
    @routes
    <script src="{{ mix('js/app.js') }}" defer></script>

{{--    <script type="text/x-mathjax-config">--}}
{{--            MathJax.Hub.Config({--}}
{{--                tex2jax: {--}}
{{--                    inlineMath: [['$','$'],['\\(','\\)']]--}}
{{--                }--}}
{{--            });--}}

{{--    </script>--}}
    <script type="text/javascript" async
            src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-AMS_CHTML"></script>

</head>
<body class="font-sans antialiased">
@inertia
</body>
</html>
