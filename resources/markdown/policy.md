# Kebijakan Privasi SUCCESS Computer Based Test Simulation - SUCCESS COBALT
_Diperbarui pada 23 Maret 2021_

Selamat datang di Kebijakan Privasi SUCCESS Computer Based Test Simulation atau selanjutnya bisa disebut dengan SUCCESS COBALT.

Jika Anda memiliki pertanyaan tambahan atau memerlukan informasi lebih lanjut tentang Kebijakan Privasi kami, jangan ragu untuk menghubungi kami.

## Tentang Kami
SUCCESS COBALT, dapat diakses dari (cobalt.success.bateh.web.id) merupakan situs yang dikelola oleh Organisasi Mahasiswa Ekstrakampus atau lebih tepatnya Organisasi Mahasiswa Daerah bernama **Surabaya Cepu College Student Association** atau biasa disebut dengan **SUCCESS**. Situs ini berada dalam situs domain milik pribadi bernama **bateh.web.id** yang menggunakan jasa _shared hosting_ dan domain dari [Niagahoster](niagahoster.co.id). 

Situs ini bertujuan untuk memberikan bantuan kepada siapapun untuk melaksanakan ujian secara online menggunakan perangkat masing-masing yang _user-friendly_ baik ketika diakses menggunakan Desktop maupun Perangkat Pintar yang digunakan baik dari sisi pemberi soal maupun peserta yang mengerjakan soal.

Salah satu prioritas utama kami adalah privasi pengunjung kami. Dokumen Kebijakan Privasi ini berisi jenis informasi yang dikumpulkan dan dicatat oleh SUCCESS Computer Based Test Simulation - SUCCESS COBALT dan bagaimana kami menggunakannya.

## Data Pengguna

Kami mengumpulkan data pribadi pengguna dengan tujuan untuk mempermudah proses penggunaan situs. Seluruh informasi pribadi akan digunakan oleh kami pribadi tanpa menyebarkan ke pihak ketiga manapun dan akan dilindungi oleh SUCCESS.

Data pribadi yang mungkin dikumpulkan oleh Bhinneka termasuk tetapi tidak terbatas pada:
1. Alamat email
2. Nama Anda

Data pribadi tersebut hanya digunakan sebagai informasi untuk pendaftaran akun dan login saja beserta informasi siapa saja yang mengerjakan soal maupun membuat soal. Selain itu kami tidak memperkenankan siapapun untuk menggunakan data di luar situs ini dengan pengecualian pihak yang menggunakan data tersebut memberikan alasan yang bisa dipertanggungjawabkan dan semua pengguna mendapatkan peringatan dan penjelasan sejelas mungkin.

## File Log/Catatan Riwayat

SUCCESS Computer Based Test Simulation - SUCCESS COBALT mengikuti prosedur stAndar menggunakan file log. File-file ini mencatat pengunjung ketika mereka mengunjungi situs web. Semua perusahaan hosting melakukan ini dan merupakan bagian dari analitik layanan hosting. Informasi yang dikumpulkan oleh file log termasuk alamat protokol internet (IP), jenis browser, Penyedia Layanan Internet (ISP), tanggal dan waktu, halaman rujukan / keluar, dan mungkin jumlah klik. Ini tidak terkait dengan informasi apa pun yang dapat diidentifikasi secara pribadi. Tujuan dari informasi tersebut adalah untuk menganalisis tren, mengelola situs, melacak pergerakan pengguna di situs web, dan mengumpulkan informasi demografis.

## _Cookies_

Seperti situs web lainnya, "SUCCESS Computer Based Test Simulation - SUCCESS COBALT" menggunakan '_cookies_' atau dalam bahasa biasa disebut 'kuki'. Berikut adalah kuki yang kami terapkan dalam situs:
1. Sesi akun Anda. Kuki ini digunakan untuk mengidentifikasi bahwa Anda telah masuk ke situs dan situs dapat memproses permintaan Anda.
1. Token XSRF. Kuki ini digunakan untuk memastikan keamanan penjelajahan pengunjung dengan mencegah pemalsuan permintaan lintas situs. Kuki ini penting untuk keamanan situs web dan pengunjung.


## Pendistribusian Informasi ke Pihak Ketiga

Seperti yang sudah dijelaskan pada bagian **Data Pengguna**, bahwa kami tidak akan menjual, mendistribusikan atau menyewakan informasi yang Anda berikan ke situs kami kepada siapapun kecuali terbatas untuk hal-hal berikut:
1. Apabila diwajibkan dan/atau diminta oleh institusi yang berwenang berdasarkan ketentuan hukum yang berlaku, somasi, perintah resmi dari Pengadilan, dan/atau perintah resmi dari instansi atau aparat yang bersangkutan, termasuk namun tidak terbatas pada perselisihan, penyelidikan, penyidikan, proses hukum dan proses penyelesaian sengketa antara SUCCESS dengan Anda, antara pengguna layanan SUCCESS, dan Pengguna dengan pihak lainnya serta kegiatan ilegal lainnya
1. Apabila diperlukan oleh partner SUCCESS yang telah dipercaya oleh anggotanya dalam rangka melaksanakan kegiatan yang membutuhkan bantuan dengan situs kami, namun dengan perjanjian yang jelas dan transparansi dengan pengguna kami.

Selain untuk kepentingan-kepentingan sebagaimana disebutkan di atas, Anda akan menerima pemberitahuan apabila akan terjadi pendistribusian informasi pribadi Anda kepada pihak lain. SUCCESS mengungkapkan informasi Anda dalam upaya mematuhi kewajiban hukum dan/atau adanya permintaan yang sah dari aparat penegak hukum.

Kami juga tidak menggunakan kuki dari pihak ketiga manapun termasuk periklanan karena kami tidak menggunakan iklan dari manapun untuk ditampilkan di dalam situs COBALT ini.

## Kemanan
SUCCESS berkomitmen untuk memastikan bahwa informasi yang Anda berikan kepada SUCCESS dalam keadaan aman. Untuk mencegah akses tidak sah, SUCCESS melakukan tindakan pengamanan fisik, elektronik, dan prosedur manajerial yang diperlukan untuk melindungi informasi yang Anda berikan.

## Penyimpanan dan Penghapusan Data
SUCCESS berhak menyimpan informasi pribadi selama Anda masih menjadi pengguna situs baik di dalam wilayah Republik Indonesia maupun di luar wilayah Republik Indonesia dan Bhinneka juga dapat melakukan penghapusan sesuai dengan ketentuan hukum yang berlaku. Dan kami pihak admin berhak menghapus dan memblokir akun apabila akun tersebut terbukti berusaha menggunakan situs untuk keperluan tidak terpuji seperti melakukan _mining_ tentang informasi anggota maupun ujian yang tersimpan di situs.

## Penerapan Kebijakan Privasi Ini

Kebijakan Privasi ini hanya berlaku untuk aktivitas online kami dan berlaku untuk pengunjung situs web kami sehubungan dengan informasi yang mereka bagikan dan/atau kumpulkan dalam "SUCCESS Computer Based Test Simulation - SUCCESS COBALT". Kebijakan ini tidak berlaku untuk informasi apa pun yang dikumpulkan secara offline atau melalui saluran selain situs web ini.

## Consent

Dengan menggunakan situs web kami, Anda dengan ini menyetujui Kebijakan Privasi kami dan menyetujui Syarat dan Ketentuannya.
Segala pertanyaan, masalah, atau keluhan mengenai Kebijakan Privasi ini dapat menghubungi admin di webmaster@bateh.web.id.
