# Syarat dan Ketentuan SUCCESS Computer Based Test Simulation - SUCCESS COBALT

Selamat datang di situs SUCCESS Computer Based Test Simulation atau selanjutnya bisa disebut dengan SUCCESS COBALT.

Berikut adalah Syarat dan Ketentuan yang memuat mengenai seluruh peraturan dan ketentuan yang mengikat Anda secara otomatis pada saat Anda melakukan kegiatan seperti, berkunjung, melakukan pendaftaran sebagai pengguna. Anda diharuskan untuk membaca, memahami, menerima dan menyetujui semua persyaratan dan ketentuan dalam perjanjian ini sebelum menggunakan situs dan/atau menerima konten yang terdapat di dalamnya. Dengan mengakses atau menggunakan situs ini, pengguna dianggap telah memahami dan menyetujui seluruh isi dalam syarat dan ketentuan di bawah ini. Syarat dan ketentuan dapat diubah atau diperbaharui sewaktu-waktu tanpa ada pemberitahuan terlebih dahulu. Perubahan syarat dan ketentuan akan segera berlaku setelah dicantumkan di dalam situs ini. Jika pengguna merasa keberatan terhadap syarat dan ketentuan yang SUCCESS ajukan dalam Perjanjian ini, maka kami anjurkan untuk tidak menggunakan situs ini.

## Ketentuan Penggunaan Situs

Saat mengunjungi dan menggunakan situs SUCCESS, termasuk setiap fitur dan layanannya, Setiap pengguna wajib untuk mematuhi ketentuan pengguna situs berikut ini:
1. Akses situs ini hanya diperkenankan untuk keperluan media kuis atau evaluasi peserta.
1. Pengguna tidak diperkenankan untuk mempromosikan produk apapun dalam situs ini.
1. Pengguna tidak diperkenankan untuk memuat pertanyaan/jawaban apapun yang mengandung SARA (Suku, Agama, Ras, Aparheid) maupun mengancam, cabul, tidak senonoh, pornografi atau bisa menimbulkan segala kewajiban hukum perdata atau pidana Indonesia atau hukum internasional.
1. Pengguna setuju bahwa selama menggunakan layanan SUCCESS COBALT atas risiko pengguna sendiri dan layanan SUCCESS COBALT diberikan kepada pengguna dengan kondisi "sebagaimana adanya" dan "sebagaimana tersedia".
1. SUCCESS berhak untuk mengganti, mengubah, menangguhkan atau menghentikan semua atau bagian apapun dari Situs ini atau Layanan setiap saat atau setelah memberikan pemberitahuan sebagaimana dipersyaratkan oleh undang-undang dan peraturan yang berlaku. SUCCESS juga dapat meluncurkan Layanan tertentu atau fitur tertentu dalam versi beta, yang mungkin tidak berfungsi dengan baik atau sama seperti versi akhir, dan SUCCESS tidak bertanggung jawab dalam hal demikian. SUCCESS juga dapat membatasi fitur tertentu atau membatasi akses anda ke bagian atau seluruh Situs atau Layanan atas kebijakannya sendiri dan tanpa pemberitahuan atau kewajiban.

## Hak Milik Intelektual
Surabaya Cepu College Student Association adalah pemilik tunggal atau pemegang sah semua hak atas situs dalam konten situs SUCCESS COBALT. Pemilik situs bateh.web.id juga tidak memiliki hak atas situs ini. 

## Akun Pengguna
1. Pendaftaran akun di SUCCESS COBALT tidak dipungut biaya/gratis.
2. Ketika membuat akun, pengguna akan meminta nama, email, dan password untuk menyelesaikan proses registrasi.
3. SUCCESS tanpa pemberitahuan terlebih dahulu kepada Pengguna, memiliki kewenangan untuk melakukan tindakan yang perlu atas setiap dugaan pelanggaran atau pelanggaran Syarat & ketentuan dan/atau hukum yang berlaku, yakni tindakan berupa suspensi atau penghapusan akun.
1. Member SUCCESS COBALT bertanggung jawab untuk menjaga kerahasiaan dan keamanan atas nama akun dan password serta bertanggung jawab sepenuhnya atas segala kegiatan yang diatasnamakan nama akun Pengguna Terdaftar.
1. Member SUCCESS COBALT berhak memberitahukan kepada kami apabila ada dugaan penyalahgunaan akun Anda.
1. Kami berhak sepenuhnya untuk membatasi, memblokir atau mengakhiri pelayanan dari suatu akun, melarang akses ke situs SUCCESS COBALT dan konten, layanan, dan memperlambat atau menghapus hosted content, dan mengambil langkah-langkah hukum untuk menjaga member SUCCESS COBALT atau pengguna lain jika kami menganggap member SUCCESS COBALT atau pengguna lain melanggar hukum-hukum yang berlaku, melanggar hak milik intelektual dari pihak terkait, atau melakukan suatu pelanggaran yang melanggar hal-hal yang tertera pada Perjanjian ini.
1. Kami tidak pernah meminta password akun, sms maupun email verifikasi, atau OTP milik akun pengguna untuk alasan apapun.
1. Pengguna tidak diperkenankan menyerahkan maupun mengalihkan akun ke pengguna lain dalam kondisi apapun.

# Konten Buatan Pengguna
Seluruh konten baik ujian, paket, pertanyaan, maupun atribut soal yang tersedia di dalam layanan ini adalah hak milik pembuat masing-masing paket. Kami SUCCESS tidak berhak menyalin maupun merubah apapun yang telah dibuat pengguna kami dengan pengecualian kami telah membuat persetujuan hanya untuk mempublikasikan konten yang bersangkutan dengan pembuatnya.  tidak pernah meminta password akun, sms verifikasi, atau OTP milik akun pengguna untuk alasan apapun.
