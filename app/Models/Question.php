<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;
    protected $fillable = [
        'question',
        'code',
        'number',
        'score',
        'question_group_id',
        'question_attribute_id',
        'updated_by'
    ];

    public function question_group(){
        return $this->belongsTo(QuestionGroup::class);
    }

    public function user(){
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function question_attribute(){
        return $this->belongsTo(QuestionAttribute::class);
    }

    public function answer_option(){
        return $this->hasMany(AnswerOption::class);
    }
}
