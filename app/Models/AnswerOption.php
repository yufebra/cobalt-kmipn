<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnswerOption extends Model
{
    use HasFactory;
    protected $fillable = [
        'answer',
        'code',
        'order',
        'is_correct',
        'question_id',
        'updated_by'
    ];

    public function question(){
        return $this->belongsTo(Question::class);
    }

    public function user(){
        return $this->belongsTo(User::class, "updated_by");
    }

    public function user_question_answer(){
        return $this->hasMany(UserQuestionAnswer::class);
    }
}
