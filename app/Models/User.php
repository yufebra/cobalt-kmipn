<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
    //implements MustVerifyEmail
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'type',
        'validated'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    public function user_right(){
        return $this->hasMany(UserRight::class);
    }

    public function test_created_by(){
        return $this->hasOne(Test::class, 'created_by');
    }

    public function test_updated_by(){
        return $this->hasOne(Test::class, 'updated_by');
    }

    public function question_group_created_by(){
        return $this->hasOne(QuestionGroup::class, 'created_by');
    }

    public function question_group_updated_by(){
        return $this->hasOne(QuestionGroup::class, 'updated_by');
    }

    public function question(){
        return $this->hasOne(Question::class, 'updated_by');
    }

    public function question_attribute(){
        return $this->hasOne(QuestionAttribute::class, 'updated_by');
    }

    public function answer_option(){
        return $this->hasOne(AnswerOption::class, 'updated_by');
    }

    public function user_session(){
        return $this->hasMany(UserQuestionSession::class);
    }

    public function user_grant(){
        return $this->hasMany(UserRight::class, 'granted_by');
    }
}
