<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserFeedback extends Model
{
    use HasFactory;
    protected $fillable = [
        'difficulty',
        'hardest',
        'feedback',
        'critic',
        'suggestion',
        'question_group_id'
    ];

    public function question_group(){
        return $this->belongsTo(QuestionGroup::class);
    }
}
