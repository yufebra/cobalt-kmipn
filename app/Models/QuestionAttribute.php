<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestionAttribute extends Model
{
    use HasFactory;
    protected $fillable = [
        'attribute',
        'code',
        'command',
        'order',
        'question_group_id',
        'updated_by',
    ];

    public function question(){
        return $this->hasMany(Question::class);
    }

    public function question_group(){
        return $this->belongsTo(QuestionGroup::class);
    }

    public function user(){
        return $this->belongsTo(User::class, 'updated_by');
    }
}
