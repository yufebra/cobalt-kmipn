<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRight extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'test_id',
        'granted_by'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function granted_by(){
        return $this->belongsTo(User::class, 'granted_by');
    }

    public function test(){
        return $this->belongsTo(Question::class);
    }
}
