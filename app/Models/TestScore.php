<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TestScore extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'user_question_session_id',
        'correct',
        'wrong',
        'empty'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function user_question_session(){
        return $this->belongsTo(UserQuestionSession::class);
    }
}
