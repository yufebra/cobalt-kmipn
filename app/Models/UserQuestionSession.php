<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserQuestionSession extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'test_id',
        'status',
        'question_group_id',
        'feedback',
        'start_time',
        'end_time'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function test(){
        return $this->belongsTo(Test::class);
    }

    public function question_group(){
        return $this->belongsTo(QuestionGroup::class);
    }

    public function test_score(){
        return $this->hasMany(TestScore::class);
    }

    public function user_question_answer(){
        return $this->hasMany(UserQuestionAnswer::class);
    }
}
