<?php

namespace App\Http\Controllers;

use App\Models\AnswerOption;
use App\Models\Question;
use Illuminate\Http\Request;
use Inertia\Inertia;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // New or edit
        //dd($request->json());
        if(empty($request->json('id'))){
            $total = Question::where('question_group_id', '=', $request->json('question_group_id'))->get()->count();
            $target = Question::with(['answer_option'])->create([
                'code' => $request->json('code'),
                'question_group_id' => $request->json('question_group_id'),
                'number' => $total+1,
                'question' => $request->json('question'),
                'question_attribute_id' => $request->json('question_attribute_id'),
                'score' => $request->json('score'),
                'updated_by' => auth()->id(),
            ]);
            dd($target);
        }
        else{
            $target = Question::with(['answer_option'])->where('code', '=', $request->json('code'))->first();
            $target->question = $request->json('question');
            $target->number = $request->json('number');
            $target->updated_by = auth()->id();
            $target->save();
        }
        
        
        // $target = Question::with(['answer_option'])->updateOrCreate([
        //     'code' => $request->json('code'),
        //     'question_group_id' => $request->json('question_group_id'),
        // ], [
        //     'number' => $total,
        //     'question' => $request->json('question'),
        //     'question_attribute_id' => $request->json('question_attribute_id'),
        //     'score' => $request->json('score'),
        //     'updated_by' => auth()->id(),
        // ]);

        foreach ($request->json('answer_option') as $item){
            $data = AnswerOption::updateOrCreate([
                'question_id' => $target->id,
                'code' => $item['code'],
            ],[
                'is_correct' => $item['is_correct'],
                'answer' => $item['answer']
            ]);
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $target = Question::findOrFail($id);
        $target->delete();

        $datas = Question::where([
            'question_group_id' => $target->question_group_id,
        ])->orderBy('number')->get();

        foreach($datas as $index=>$data){
            $data->number = $index+1;
            $data->save();
        }

        return redirect()->back();
    }

    public function reorder(Request $request){
        foreach ($request->json('question') as $index=>$item){
            $target = Question::with(['answer_option'])->updateOrCreate([
                'question_group_id' => $item['question_group_id'],
                'question' => $item['question'],
                'question_attribute_id' => $item['question_attribute_id'],
                'score' => $item['score'],
                'code' => $item['code'],
            ], [
                'number' => $index+1,
                'updated_by' => auth()->id(),
            ]);

            if($target->code == null){
                $randoms = '1234567890QWERTYUIOPASDFGHJKLZXCVBNM1234567890QWERTYUIOPASDFGHJKLZXCVBNM1234567890QWERTYUIOPASDFGHJKLZXCVBNM';
                $code = substr(str_shuffle($randoms), 0, 8);
                $target->code = $code;
                $target->save();
            }

            foreach ($target->answer_option as $ans){
                if($ans->code == null){
                    $randoms = '1234567890QWERTYUIOPASDFGHJKLZXCVBNM1234567890QWERTYUIOPASDFGHJKLZXCVBNM1234567890QWERTYUIOPASDFGHJKLZXCVBNM';
                    $code = substr(str_shuffle($randoms), 0, 8);
                    $ans->code = $code;
                    $ans->save();
                }
            }
        }

        return redirect()->back();
    }

    public function deleteAnswer($id)
    {
        $target = AnswerOption::findOrFail($id);
        $target->delete();

        return redirect()->back();
    }
}
