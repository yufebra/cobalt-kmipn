<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;

class HomeController extends Controller
{
    public function welcome(){
        return Inertia::render('Welcome');
    }

    public function dashboard(){
        return Inertia::render('Dashboard');
    }

    public function adminRegister(){
        return Inertia::render('Host/Auth/AdminRegister');
    }

    public function changelog(){
        return Inertia::render('Changelog');
    }
}
