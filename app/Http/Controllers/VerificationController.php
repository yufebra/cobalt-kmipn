<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;

class VerificationController extends Controller
{
    public function index(){
        $unvalidatedParticipant = User::where([
            'validated' => 0,
            'type' => 'participant'
        ])->get();
        $unvalidatedHost = User::where([
            'validated' => 0,
            'type' => 'host'
        ])->get();
        $validatedParticipant = User::where([
            'validated' => 1,
            'type' => 'participant'
        ])->get();
        $validatedHost = User::where([
            'validated' => 1,
            'type' => 'host'
        ])->get();
        return Inertia::render('Host/UserManagement/UserVerify', [
            'unvalidatedParticipants' => $unvalidatedParticipant,
            'unvalidatedHosts' => $unvalidatedHost,
            'participants' => $validatedParticipant,
            'hosts' => $validatedHost
        ]);
    }

    public function verifyUser(Request $request){
        $user = User::findOrFail($request->json('id'));
        $user->update([
            'validated' => 1,
        ]);

        return redirect()->back();
    }
}
