<?php

namespace App\Http\Controllers;

use App\Models\AnswerOption;
use App\Models\QuestionGroup;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Symfony\Component\Console\Input\Input;

class QuestionGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Host/RoomManagement/QuestionEdit');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $create = QuestionGroup::create([
            'name' => $request->json('name'),
            'test_id' => $request->json('test_id'),
            'created_by' => auth()->id(),
            'updated_by' => auth()->id(),
        ]);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($code, $id)
    {
        $target = QuestionGroup::with([
            'question' => function ($q){
                return $q->orderBy('number', 'asc');
            },
            'question_attribute' => function ($q){
                return $q->orderBy('order', 'asc');
            },
            'question.user',
            'question.answer_option',
            'question_attribute.user'
        ])->findOrFail($id);



//        dd($target);

        return Inertia::render('Host/RoomManagement/QuestionEdit', [
            'test' => $target,
            'code' => $code,
            'package_id' => $id,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $target = QuestionGroup::findOrFail($id);
        $target->name = $request->json('name');
        $target->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $target = QuestionGroup::findOrFail($id);
        $target->delete();

        return redirect()->back();
    }

    public function transfer(Request $request){
        $target = QuestionGroup::findOrFail($request->json('pack_id'));

        $target->name = $request->json('name');
        $target->test_id = $request->json('test_id');
        $target->save();

        return redirect()->back();
    }

    private function generateCode($len = 8){
        $randoms = '1234567890QWERTYUIOPASDFGHJKLZXCVBNM1234567890QWERTYUIOPASDFGHJKLZXCVBNM1234567890QWERTYUIOPASDFGHJKLZXCVBNM';
        return substr(str_shuffle($randoms), 0, $len);
    }

    public function copy(Request $request){
        $target = QuestionGroup::with([
            'question_attribute',
            'question.answer_option'
        ])->findOrFail($request->json('pack_id'));

        $newPack = $target->replicate()->fill([
            'name' => $request->json('name'),
            'test_id' => $request->json('test_id'),
            'created_by' => auth()->id(),
            'updated_by' => auth()->id(),
        ]);
        $newPack->save();

        $attributes = [];

        foreach ($target->question_attribute as $q){
            $new = $q->replicate()->fill([
                'code' => $this->generateCode(8),
                'question_group_id' => $newPack->id,
                'updated_by' => auth()->id(),
            ]);

            $new->save();
            $attributes[$new->id] = $q->id;
        }


        foreach ($target->question as $q){
            $new = $q->replicate()->fill([
                'code' => $this->generateCode(8),
                'question_group_id' => $newPack->id,
                'updated_by' => auth()->id(),
            ]);

            if($q->question_attribute_id != null){
                $new->question_attribute_id = array_search($q->question_attribute_id, $attributes);
            }

            $new->save();

            foreach ($q->answer_option as $a){
                $newAns = $a->replicate()->fill([
                    'code' => $this->generateCode(8),
                    'question_id' => $new->id,
                ]);

                $newAns->save();
            }
        }

        return redirect()->back();
    }
}
