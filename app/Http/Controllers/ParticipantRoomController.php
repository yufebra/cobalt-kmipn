<?php

namespace App\Http\Controllers;

use App\Models\QuestionGroup;
use App\Models\Test;
use App\Models\TestScore;
use App\Models\User;
use App\Models\UserFeedback;
use App\Models\UserQuestionAnswer;
use App\Models\UserQuestionSession;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ParticipantRoomController extends Controller
{
    public function checkRoom(Request $request){
        $room = Test::where('code', '=', $request->json('code'))->first();
        return (!empty($room)) ? response()->json(['found' => true]) : response()->json(['found' => false]);
    }

    public function prepareTheTest($room){
        $que = [];

        foreach ($room->question_group as $item){
            array_push($que, $item);
        }

        $index = array_rand($que);

        return UserQuestionSession::create([
            'user_id' => auth()->user()->id,
            'test_id' => $room->id,
            'status' => 'not',
            'question_group_id' => $que[$index]->id,
        ]);
    }

    public function waitingRoom($code){
        $room = Test::with('question_group')->where('code', '=', $code)->firstOrFail();
        $acceptedStatus = ['not', 'started'];
        $check = UserQuestionSession::where([
            ['user_id', '=', auth()->id()],
            ['test_id', '=', $room->id],
        ])->whereIn('status', $acceptedStatus)->orderBy('created_at', 'desc')->first();

        if(empty($check)) $check = $this->prepareTheTest($room);

        $max = 0;
        foreach ($room->question_group as $quest){
            $max = max($quest->question->count(), $max);
        }

        return Inertia::render('Participant/TestRoom/WaitingRoom', ['test' => $room, 'max' => $max]);
    }

    public function testRoom($code){
        $room = Test::with('question_group')->where('code', '=', $code)->firstOrFail();
        $acceptedStatus = ['not', 'started'];
        $check = UserQuestionSession::where([
            ['user_id', '=', auth()->id()],
            ['test_id', '=', $room->id],
        ])->whereIn('status', $acceptedStatus)->orderBy('created_at', 'desc')->first();

        if(empty($check)){
            return $this->waitingRoom($code);
        }

        if($check->status == 'not'){
            $check->status = 'started';
            $check->start_time = now()->format('Y-m-d H:i:s');
            $check->save();
        }

        $test = QuestionGroup::with([
            'question' => function ($q){
                return $q->orderBy('number', 'asc');
            },
            'question.answer_option' => function ($q){
                return $q->inRandomOrder();
            },
            'question.answer_option.user_question_answer' => function ($q) use ($check){
                return $q->where('user_question_session_id', '=', $check->id);
            },
            'question.question_attribute'
        ])->findOrFail($check->question_group_id);

        foreach ($test->question as $item){
            $item->answer_option->each(function ($i, $k){
                $i->makeHidden('is_correct');
            });
        }

        $current = Carbon::now();

        if($current->gt(Carbon::parse($room->start)) && $current->lt(Carbon::parse($room->end))) {
            return Inertia::render('Participant/TestRoom/MainRoom', [
                'test' => $test,
                'end' => $room->end,
                'room' => $room,
                'my_session' => $check->id,
            ]);
        }else{
            return redirect()->route('feedback', ['code' => $room->code]);
        }
    }

    public function userAnswer(Request $request){
//        dd($request);
        $target = UserQuestionAnswer::updateOrCreate([
            'user_question_session_id' => $request->json('session_id'),
            'question_id' => $request->json('question_id')
        ],[
            'answer_option_id' => $request->json('answer')
        ]);

        return redirect()->back();
    }

    public function feedbackRoom($code){
        $room = Test::where('code', '=', $code)->firstOrFail();
        $acceptedStatus = ['finished', 'started'];
        $check = UserQuestionSession::with(['test', 'question_group'])->where([
            ['user_id', '=', auth()->id()],
            ['test_id', '=', $room->id],
        ])->whereIn('status', $acceptedStatus)->orderBy('created_at', 'desc')->firstOrFail();

        if($check->status == 'started'){
            $check->status = 'finished';
            $check->end_time = now()->format('Y-m-d H:i:s');
            $check->save();
        }

        $test = QuestionGroup::with([
            'question' => function ($q){
                return $q->orderBy('number', 'asc');
            },
            'question.answer_option.user_question_answer' => function ($q) use ($check){
                return $q->where('user_question_session_id', '=', $check->id);
            },
            'question.question_attribute'
        ])->findOrFail($check->question_group_id);

        $total = $test->question->count();
        $correct = 0;
        $wrong = 0;

        // Untuk setiap pertanyaan
        foreach ($test->question as $question){

            // Untuk setiap opsi jawaban di pertanyaan
            foreach ($question->answer_option as $option) {

                // Apakah ada yg menjawab opsi ini
                if($option->user_question_answer->isNotEmpty()){

                    // Jika sesi di database itu sesuai dengan sesi user sekarang
                    if ($option->user_question_answer[0]->user_question_session_id == $check->id){

                        // Jika opsi ini adalah yang benar
                        if($option->is_correct == 1){
                            $correct++;
                            break;
                        }
                        // Jika yang salah
                        else{
                            $wrong++;
                            break;
                        }
                    }
                }
            }
        }

        $result = [
            'correct' => $correct,
            'wrong' => $wrong,
            'empty' => $total-($correct+$wrong)
        ];

        if($check->wasChanged()){
            $score = TestScore::create([
                'user_id' => auth()->id(),
                'user_question_session_id' => $check->id,
                'correct' => $correct,
                'wrong' => $wrong,
                'empty' => $result['empty'],
            ]);
        }

        return Inertia::render('Participant/TestRoom/FeedbackRoom', [
            'room' => $check,
            'result' => $result,
            'test_id' => $test->id,
        ]);
    }

    public function sendFeedback(Request $request){
        $new = UserFeedback::create([
            'question_group_id' => $request->json('question_group_id'),
            'difficulty' => $request->json('rating'),
            'hardest' => $request->json('difficult'),
            'feedback' => $request->json('feedback'),
            'critic' => $request->json('critic'),
            'suggestion' => $request->json('suggestion')
        ]);

        return redirect()->route('dashboard');
    }
}
