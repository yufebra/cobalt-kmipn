<?php

namespace App\Http\Controllers;

use App\Models\QuestionAttribute;
use Illuminate\Http\Request;

class QuestionAttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(empty($request->json('id'))){
            $total = Question::where('question_group_id', '=', $request->json('question_group_id'))->get()->count();
            $target = QuestionAttribute::create([
                'code' => $request->json('code'),
                'question_group_id' => $request->json('question_group_id'),
                'order' => $total+1,
                'command' => $request->json('command'),
                'attribute' => $request->json('attribute'),
                'updated_by' => auth()->id()
            ]);
        }
        // Edit data
        else{
            $target = QuestionAttribute::where('code', '=', $request->json('code'))->first();
            $target->attribute = $request->json('attribute');
            $target->updated_by = auth()->id();
            $target->save();
        }
        
        // $target = QuestionAttribute::updateOrCreate([
        //     'code' => $request->json('code'),
        //     'question_group_id' => $request->json('question_group_id'),
        // ],[
        //     'order' => $total,
        //     'command' => $request->json('command'),
        //     'attribute' => $request->json('attribute'),
        //     'updated_by' => auth()->id()
        // ]);

        return redirect()->back();
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $target = QuestionAttribute::findOrFail($id);
        $target->delete();

        $datas = QuestionAttribute::where([
            'question_group_id' => $target->question_group_id,
        ])->orderBy('order')->get();

        foreach($datas as $index=>$data){
            $data->order = $index+1;
            $data->save();
        }

        return redirect()->back();
    }

    public function reorder(Request $request){
        foreach ($request->json('question_attribute') as $index=>$item){
            $target = QuestionAttribute::updateOrCreate([
                'attribute' => $item['attribute'],
                'command' => $item['command'],
                'question_group_id' => $item['question_group_id'],
                'code' => $item['code'],
            ], [
                'order' => $index+1,
                'updated_by' => auth()->id(),
            ]);

            if($target->code == null){
                $randoms = '1234567890QWERTYUIOPASDFGHJKLZXCVBNM1234567890QWERTYUIOPASDFGHJKLZXCVBNM1234567890QWERTYUIOPASDFGHJKLZXCVBNM';
                $code = substr(str_shuffle($randoms), 0, 8);
                $target->code = $code;
                $target->save();
            }
        }

        return redirect()->back();
    }
}
