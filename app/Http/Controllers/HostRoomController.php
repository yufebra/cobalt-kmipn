<?php

namespace App\Http\Controllers;

use App\Models\QuestionGroup;
use App\Models\Test;
use App\Models\User;
use App\Models\UserRight;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Inertia\Inertia;

class HostRoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        $me = auth()->user()->id;
        $tests = Test::with(['user_right', 'user_created_by', 'user_updated_by'])->whereHas('user_right', function ($query) use ($me) {
            $query->where('user_id', $me);
        })->get();
        return Inertia::render('Host/RoomManagement/RoomList', ['tests' => $tests]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return string
     */
    public function store(Request $request)
    {
        $randoms = '1234567890QWERTYUIOPASDFGHJKLZXCVBNM1234567890QWERTYUIOPASDFGHJKLZXCVBNM1234567890QWERTYUIOPASDFGHJKLZXCVBNM';
        $code = substr(str_shuffle($randoms), 0, 6);
        $create = Test::create([
            'name' => $request->json('name'),
            'description' => $request->json('description'),
            'start' => $request->json('start'),
            'end' => $request->json('end'),
            'researcher' => $request->json('researcher'),
            'is_open' => 0,
            'code' => $code,
            'created_by' => auth()->user()->id,
            'updated_by' => auth()->user()->id,
        ]);

        $authUser = UserRight::create([
            'user_id' => auth()->user()->id,
            'test_id' => $create->id,
            'granted_by' => auth()->user()->id,
        ]);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * //     * @param int $id
     * @return \Inertia\Response
     */
    public function show($code)
    {
        $testData = Test::with([
            'user_created_by',
            'user_updated_by',
            'user_right.user',
            'user_right.granted_by',
            'question_group.user_created_by',
            'question_group.user_updated_by',
            'question_group.question',
            'question_group.question_attribute'
        ])->where('code', $code)->firstOrFail();

        $tests = Test::all();
        $dropdown = [];
        foreach ($tests as $item){
            $temp['id'] = $item->id;
            $temp['text'] = $item->name;
            array_push($dropdown, $temp);
        }

        $found = false;
        foreach ($testData->user_right as $user){
            if($user->user_id == auth()->id()){
                $found = true;
                break;
            }
        }

        if(!$found) return redirect()->route('host.room');

        $users = User::where([
            'type' => 'host',
            'validated' => 1,
        ])->get();

        return Inertia::render('Host/RoomManagement/RoomEdit', [
            'test' => $testData,
            'users' => $users,
            'tests' => $dropdown
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Test::findOrFail($id);
        $data->name = $request->json('name');
        $data->description = $request->json('description');
        $data->start = $request->json('start');
        $data->end = $request->json('end');
        $data->researcher = $request->json('researcher');
        $data->save();

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function availability(Request $request, $id)
    {
        $data = Test::findOrFail($id);
        $data->is_open = ($request->json('is_open') == 1) ? 0 : 1;
        $data->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = Test::findOrFail($id);
        $result->delete();

        return redirect()->route('host.room');
    }

    public function participant($code){
        $test = Test::with([
            'user_right',
            'question_group.user_question_session.user',
            'question_group.user_question_session' => function ($q){
                return $q->orderBy('created_at', 'asc');
            },
        ])->where(['code' => $code])->firstOrFail();

        $found = false;
        foreach ($test->user_right as $user){
            if($user->user_id == auth()->id()){
                $found = true;
                break;
            }
        }

        if(!$found) return redirect()->route('host.room');
        return Inertia::render('Host/RoomFacts/Participants', ['test' => $test]);
    }

    public function analyze($code){
//        return "Sabar ya... Masih dibuat :\")";

        $test = Test::with([
            'question_group.user_question_session' => function($q){
                return $q->where('status', '=', 'finished');
            },
            'question_group.user_question_session.user',
            'question_group.user_question_session.test_score',
            'question_group.user_question_session.user_question_answer.answer_option.question' => function ($q){
                return $q->orderBy('number', 'asc');
            },
            'question_group.question.answer_option.user_question_answer.user_question_session'
        ])->where(['code' => $code])->firstOrFail();

        //dd($test);

        $found = false;
        foreach ($test->user_right as $user){
            if($user->user_id == auth()->id()){
                $found = true;
                break;
            }
        }

        if(!$found) return redirect()->route('host.room');

        $total = 0;
        $scores = [
            'participants' => 0,
            'correct' => [
                'averageScore' => 0,
                'highest' => 0,
                'highestBy'=> ''
            ],
            'wrong' => [
                'averageScore' => 0,
                'highest' => 0,
                'highestBy'=> ''
            ],
            'empty' => [
                'averageScore' => 0,
                'highest' => 0,
                'highestBy'=> ''
            ],
        ];

        $packs = [];
        $leaderboard = [];

        foreach ($test->question_group as $pack){
            $sessions = $pack->user_question_session->count();
            $total += $sessions;

            // Counting Average of Every Packages
            $packageInfo = [
                'name' => $pack->name,
                'participants' => $sessions,
                'correct' => [
                    'averageScore' => 0,
                    'highest' => 0,
                    'highestBy' => null,
                ],
                'wrong' => [
                    'averageScore' => 0,
                    'highest' => 0,
                    'highestBy' => null,
                ],
                'empty' => [
                    'averageScore' => 0,
                    'highest' => 0,
                    'highestBy' => null,
                ],
                'hardest' => [
                    'question' => null,
                    'totalCorrect' => 0,
                    'totalWrong' => 0,
                    'totalEmpty' => 0,
                ],
                'easiest' => [
                    'question' => null,
                    'totalCorrect' => 0,
                    'totalWrong' => 0,
                    'totalEmpty' => 0,
                ],
                'mostSkipped' => [
                    'question' => null,
                    'totalCorrect' => 0,
                    'totalWrong' => 0,
                    'totalEmpty' => 0,
                ],
                'questions' => [],
                'userRank' => [],
            ];
            $id = 0;

            foreach ($pack->user_question_session as $session){
                foreach ($session->test_score as $score){
                    $temp = [
                        'id' => ++$id,
                        'pack' => $pack->name,
                        'correct' => $score->correct,
                        'wrong' => $score->wrong,
                        'empty' => $score->empty,
                        'session' => $session,
                    ];
                    array_push($packageInfo['userRank'], $temp);
                    array_push($leaderboard, $temp);
                    $packageInfo['correct']['averageScore'] += $score->correct;
                    $packageInfo['wrong']['averageScore'] += $score->wrong;
                    $packageInfo['empty']['averageScore'] = $score->empty;
                    if($packageInfo['correct']['highest'] <= $score->correct){
                        $packageInfo['correct']['highest'] = $score->correct;
                        $packageInfo['correct']['highestBy'] = $session->user;
                        if($scores['correct']['highest'] <= $score->correct){
                            $scores['correct']['highest'] = $score->correct;
                            $scores['correct']['highestBy'] = $session->user;
                        }
                    }
                    if($packageInfo['wrong']['highest'] <= $score->wrong){
                        $packageInfo['wrong']['highest'] = $score->wrong;
                        $packageInfo['wrong']['highestBy'] = $session->user;
                        if($scores['wrong']['highest'] <= $score->wrong){
                            $scores['wrong']['highest'] = $score->wrong;
                            $scores['wrong']['highestBy'] = $session->user;
                        }
                    }
                    if($packageInfo['empty']['highest'] <= $score->empty){
                        $packageInfo['empty']['highest'] = $score->empty;
                        $packageInfo['empty']['highestBy'] = $session->user;
                        if($scores['empty']['highest'] <= $score->empty){
                            $scores['empty']['highest'] = $score->empty;
                            $scores['empty']['highestBy'] = $session->user;
                        }
                    }
                }
            }

            usort($packageInfo['userRank'], function ($a, $b){
                if($a['correct'] == $b['correct']) return 0;
                return ($a['correct'] < $b['correct']) ? 1 : -1;
            });

            $packageInfo['correct']['averageScore'] = ($sessions == 0) ? 0 : $packageInfo['correct']['averageScore']/$sessions;
            $packageInfo['wrong']['averageScore'] = ($sessions == 0) ? 0 : $packageInfo['wrong']['averageScore']/$sessions;
            $packageInfo['empty']['averageScore'] = ($sessions == 0) ? 0 : $packageInfo['empty']['averageScore']/$sessions;

            $scores['correct']['averageScore'] += $packageInfo['correct']['averageScore'];
            $scores['wrong']['averageScore'] += $packageInfo['wrong']['averageScore'];
            $scores['empty']['averageScore'] += $packageInfo['empty']['averageScore'];

            // Finding Most Difficult Question
            foreach ($pack->question as $q){
                $tempQuestion = [
                    'id' => $q->id,
                    'number' => $q->number,
                    'correct' => [
                        'total' => 0,
                        'rate' => 0,
                    ],
                    'wrong' => [
                        'total' => 0,
                        'rate' => 0,
                    ],
                    'empty' => [
                        'total' => 0,
                        'rate' => 0,
                    ],
                    'answers' => [],
                    'question' => $q,
                ];

                $answerPickTotal = 0;
                foreach ($q->answer_option as $ans) {
                    foreach($ans->user_question_answer as $ses){
                        if($ses->user_question_session->status == "finished") $answerPickTotal++;
                    }
                }

                foreach ($q->answer_option as $ans) {
                    $picked = 0;
                    foreach($ans->user_question_answer as $ses){
                        if($ses->user_question_session->status == "finished") $picked++;
                    }
                    if($ans->is_correct == 1) $tempQuestion['correct']['total'] = $picked;
                    else $tempQuestion['wrong']['total'] = $picked;
                    $temp = [
                        'id' => $ans->id,
                        'totalPick' => $picked,
                        'pickRate' => ($answerPickTotal == 0) ? 0 : ($picked/$answerPickTotal)*100,
                        'answer' => $ans,
                    ];
                    array_push($tempQuestion['answers'], $temp);
                }

                $tempQuestion['correct']['rate'] = ($sessions == 0) ? 0 : ($tempQuestion['correct']['total']/$sessions)*100;
                $tempQuestion['wrong']['rate'] = ($sessions == 0) ? 0 : ($tempQuestion['wrong']['total']/$sessions)*100;
                $tempQuestion['empty']['total'] = $sessions-($tempQuestion['correct']['total'] + $tempQuestion['wrong']['total']);
                $tempQuestion['empty']['rate'] = ($sessions == 0) ? 0 : ($tempQuestion['empty']['total']/$sessions)*100;

                if($tempQuestion['correct']['total'] >= $packageInfo['easiest']['totalCorrect']){
                    $packageInfo['easiest'] = [
                        'question' => $q,
                        'totalCorrect' => $tempQuestion['correct']['total'],
                        'totalWrong' => $tempQuestion['wrong']['total'],
                        'totalEmpty' => $tempQuestion['empty']['total'],
                    ];
                }
                if($tempQuestion['wrong']['total'] >= $packageInfo['hardest']['totalWrong']){
                    $packageInfo['hardest'] = [
                        'question' => $q,
                        'totalCorrect' => $tempQuestion['correct']['total'],
                        'totalWrong' => $tempQuestion['wrong']['total'],
                        'totalEmpty' => $tempQuestion['empty']['total'],
                    ];
                }
                if($tempQuestion['empty']['total'] >= $packageInfo['mostSkipped']['totalCorrect']){
                    $packageInfo['mostSkipped'] = [
                        'question' => $q,
                        'totalCorrect' => $tempQuestion['correct']['total'],
                        'totalWrong' => $tempQuestion['wrong']['total'],
                        'totalEmpty' => $tempQuestion['empty']['total'],
                    ];
                }

                array_push($packageInfo['questions'], $tempQuestion);
            }

            // Push All
            array_push($packs, $packageInfo);
        }

        usort($leaderboard, function ($a, $b){
            if($a['correct'] == $b['correct']) return 0;
            return ($a['correct'] < $b['correct']) ? 1 : -1;
        });

        $scores['correct']['averageScore'] = ($test->question_group->count() == 0) ? 0 : $scores['correct']['averageScore']/$test->question_group->count();
        $scores['wrong']['averageScore'] = ($test->question_group->count() == 0) ? 0 : $scores['wrong']['averageScore']/$test->question_group->count();
        $scores['empty']['averageScore'] = ($test->question_group->count() == 0) ? 0 : $scores['empty']['averageScore']/$test->question_group->count();

        $scores['participants'] = $total;
//        dd($scores);

        return Inertia::render('Host/RoomFacts/Stats', [
            'test' => $test,
            'packs' => $packs,
            'allLeaderboard' => $leaderboard,
            'summary' => $scores,
        ]);
    }

    public function feedback($code){
        $test = Test::with([
            'question_group.user_feedback',
            'question_group.user_question_session'
        ])->where(['code' => $code])->firstOrFail();

        $found = false;
        foreach ($test->user_right as $user){
            if($user->user_id == auth()->id()){
                $found = true;
                break;
            }
        }

        if(!$found) return redirect()->route('host.room');
        return Inertia::render('Host/RoomFacts/Feedback', ['test' => $test]);
    }
}
