<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class HostAccessGuard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->user()->participant == "host") abort(403);
        return $next($request);
    }
}
