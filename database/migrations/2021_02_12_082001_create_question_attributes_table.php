<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_attributes', function (Blueprint $table) {
            $table->id();
            $table->string("attribute", 10000);
            $table->string("code", 8)->unique();
            $table->integer('order');
            $table->string("command")->default("Bacalah Teks Dibawah Ini");
            $table->foreignId("question_group_id")->constrained("question_groups")->onDelete("cascade");
            $table->foreignId("updated_by")->nullable()->constrained("users")->onDelete("set null");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_attributes');
    }
}
