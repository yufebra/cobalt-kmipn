<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->string("question", 10000);
            $table->string("code", 8)->unique();
            $table->integer("number");
            $table->integer("score");
            $table->foreignId("question_group_id")->constrained("question_groups")->onDelete("cascade");
            $table->foreignId("question_attribute_id")->nullable()->constrained("question_attributes")->onDelete("set null");
            $table->foreignId("updated_by")->nullable()->constrained("users")->onDelete("set null");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
