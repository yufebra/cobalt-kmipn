<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string('code', 6);
            $table->string("description", 500)->nullable();
            $table->string('researcher');
            $table->dateTime("start");
            $table->dateTime("end");
            $table->boolean("is_open")->default(0);
            $table->foreignId("created_by")->nullable()->constrained("users")->onDelete("set null");
            $table->foreignId("updated_by")->nullable()->constrained("users")->onDelete("set null");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tests');
    }
}
