<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\HostRoomController;
use App\Http\Controllers\ParticipantRoomController;
use App\Http\Controllers\QuestionAttributeController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\QuestionGroupController;
use App\Http\Controllers\UserRightController;
use App\Http\Controllers\VerificationController;
use App\Http\Middleware\IsUserValidated;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'welcome']);
Route::get('/register/admin', [HomeController::class, 'adminRegister'])->name('host.register');

Route::middleware(['auth:sanctum', 'verified', IsUserValidated::class])->group(function (){
    Route::get('/dashboard', [HomeController::class, 'dashboard'])->name('dashboard');

    Route::post('/room/check', [ParticipantRoomController::class, 'checkRoom'])->name('check');
    Route::get('/room/{code}', [ParticipantRoomController::class, 'waitingRoom'])->name('wait');
    Route::get('/room/{code}/test', [ParticipantRoomController::class, 'testRoom'])->name('begin');
    Route::post('/room/send/answer', [ParticipantRoomController::class, 'userAnswer'])->name('answer');

    Route::get('/room/{code}/end', [ParticipantRoomController::class, 'feedbackRoom'])->name('feedback');
    Route::post('/room/end/post', [ParticipantRoomController::class, 'sendFeedback'])->name('feedback.post');

    Route::middleware([\App\Http\Middleware\HostAccessGuard::class])->group(function (){
        Route::get('/changelog', [HomeController::class, 'changelog'])->name('changelog');

        Route::get('/host/tests', [HostRoomController::class, 'index'])->name('host.room');
        Route::post('/host/tests/new', [HostRoomController::class, 'store'])->name('host.room.new');
        Route::put('/host/tests/update/{id}', [HostRoomController::class, 'update'])->name('host.room.update');
        Route::put('/host/tests/availability/{id}', [HostRoomController::class, 'availability'])->name('host.room.availability');
        Route::delete('/host/tests/delete/{id}', [HostRoomController::class, 'destroy'])->name('host.room.delete');

        Route::post('/host/right/new', [UserRightController::class, 'store'])->name('host.right.new');
        Route::delete('/host/right/delete/{id}', [UserRightController::class, 'destroy'])->name('host.right.delete');

        Route::post('/host/tests/package/new', [QuestionGroupController::class, 'store'])->name('host.package.new');
        Route::post('/host/tests/package/cut', [QuestionGroupController::class, 'transfer'])->name('host.package.cut');
        Route::post('/host/tests/package/copy', [QuestionGroupController::class, 'copy'])->name('host.package.copy');
        Route::put('/host/tests/package/update/{id}', [QuestionGroupController::class, 'update'])->name('host.package.update');
        Route::delete('/host/tests/package/delete/{id}', [QuestionGroupController::class, 'destroy'])->name('host.package.delete');

        Route::get('/host/room/{code}/package/{id}', [QuestionGroupController::class, 'edit'])->name('host.package.edit');
        Route::post('/host/tests/question/new', [QuestionController::class, 'store'])->name('host.question.new');
        Route::delete('/host/tests/question/delete/{id}', [QuestionController::class, 'destroy'])->name('host.question.delete');
        Route::post('/host/tests/question/reorder', [QuestionController::class, 'reorder'])->name('host.question.reorder');
        Route::delete('/host/tests/answer/delete/{id}', [QuestionController::class, 'deleteAnswer'])->name('host.answer.delete');

        Route::post('/host/tests/attribute/new', [QuestionAttributeController::class, 'store'])->name('host.attribute.new');
        Route::delete('/host/tests/attribute/delete/{id}', [QuestionAttributeController::class, 'destroy'])->name('host.attribute.delete');
        Route::post('/host/tests/attribute/reorder', [QuestionAttributeController::class, 'reorder'])->name('host.attribute.reorder');

        Route::get('/host/room/{code}', [HostRoomController::class, 'show'])->name('host.room.edit');
        Route::get('/host/room/{code}/participant', [HostRoomController::class, 'participant'])->name('host.room.participant');
        Route::get('/host/room/{code}/analyze', [HostRoomController::class, 'analyze'])->name('host.room.analyze');
        Route::get('/host/room/{code}/feedback', [HostRoomController::class, 'feedback'])->name('host.room.feedback');

        Route::get('/host/tests/package', [QuestionGroupController::class, 'index'])->name('package.edit');

        Route::get('/host/verify', [VerificationController::class, 'index'])->name('host.verify');
        Route::post('/host/verify/validate', [VerificationController::class, 'verifyUser'])->name('host.validate');
    });
});
